import React, {useState} from 'react'
import SearchBar from '../searchbar/SearchBar'
import ProductTable from '../producttable/ProductTable'


export default function FilterableProductTable(props) {

  const [filterText, setFilterText] = useState('');
  const [inStockOnly, setInStockOnly] = useState(false);

  return (
    <div>
      <SearchBar
        filterText={filterText}
        inStockOnly={inStockOnly}
        onFilterTextChange = { (e) => setFilterText(e) }
        onInStockChange = { (e) => setInStockOnly(e) }
      />
      <ProductTable
        products={props.products}
        filterText={filterText}
        inStockOnly={inStockOnly}
      />
    </div>
  );
}